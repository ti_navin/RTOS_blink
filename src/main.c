//STM32F103ZET6 FreeRTOS Test
#include "stm32f10x.h"
#include "FreeRTOSConfig.h"
//#include "stm32f10x_it.h"
#include "mytasks.h"
#include "main.h"
//task priorities
#define mainLED_TASK_PRIORITY			( tskIDLE_PRIORITY + 1)

int main(void)
{
  //init hardware
  LEDsInit();
  xTaskCreate( vLEDFlashTask1, ( const char * ) "LED1", configMINIMAL_STACK_SIZE, NULL, mainLED_TASK_PRIORITY, NULL );
  xTaskCreate( vLEDFlashTask2, ( const char * ) "LED2", configMINIMAL_STACK_SIZE, NULL, mainLED_TASK_PRIORITY + 1, NULL );
  //xTaskCreate( vLEDFlashTask3, ( const char * ) "LED3", configMINIMAL_STACK_SIZE, NULL, mainLED_TASK_PRIORITY + 2, NULL );
  xTaskCreate( vLEDBlipTask, ( const char * ) "LEDBlip", configMINIMAL_STACK_SIZE, NULL, mainLED_TASK_PRIORITY + 3, NULL );
  //start scheduler
  vTaskStartScheduler();
  //you should never get here
  while(1)
	{ }
}

void vApplicationStackOverflowHook( xTaskHandle pxTask, signed char *pcTaskName )
{
    while (1)
    {
        /* my code. Prints stuff directly to the console*/
    }
}
